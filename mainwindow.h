#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSignalMapper>
#include <QPushButton>
#include <QVector>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void                    setDesign() const;                          // Set button placement
    void                    refreshLineResult(const QString str) const; // Actualize text box content
    void                    reset();                                    // Reset attributes and text box value

private slots:
    void                    btnNumberClicked(int nb);                   // Called when a digit button is clicked
    void                    btnOperatClicked(QString op);               // Called when an operator button is clicked
private:
    Ui::MainWindow          *ui;
    int                     result;                                     // Contains calculation result value
    int                     currentOperand;                             // Contains current uncompleted number (user still typing the number)
    bool                    opBtnClicked;                               // Prevent successive use of operators
    bool                    calcEnd;                                    // To know if the user ended is calculation (if he pressed "=")
    QSignalMapper           *sMapperNb;                                 // Mapper for digit buttons
    QSignalMapper           *sMapperOp;                                 // Mapper for operators buttons
    QVector<QPushButton*>   vecBtn;                                     // Contains all buttons
};

#endif // MAINWINDOW_H
