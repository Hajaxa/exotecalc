#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    result(0), currentOperand(0),
    opBtnClicked(true), calcEnd(false),
    sMapperNb(new QSignalMapper()),
    sMapperOp(new QSignalMapper())
{
    ui->setupUi(this);

    // 1. Set number buttons and mapping (ex: 0, 1, 2, etc.)
    for (int i = 0; i < 10; ++i)
    {
        QPushButton *btn = new QPushButton(QString::number(i));
        this->sMapperNb->setMapping(btn, i);
        this->vecBtn.push_back(btn);
        connect(btn, SIGNAL(clicked(bool)), this->sMapperNb, SLOT(map()));
    }

    // 2. Set operators buttons and mapping (ex: +, -, =, etc.)
    QString         strOperators[4] = {"+", "=", "-", "*"};

    for (int i = 0; i < 4; ++i)
    {
        QPushButton *btnOp = new QPushButton(strOperators[i]);
        this->sMapperOp->setMapping(btnOp, btnOp->text());
        this->vecBtn.push_back(btnOp);
        connect(btnOp, SIGNAL(clicked(bool)), this->sMapperOp, SLOT(map()));
        if (strOperators[i] == "-" || strOperators[i] == "*")
            btnOp->setDisabled(true);
    }
    connect(this->sMapperNb, SIGNAL(mapped(int)), this, SLOT(btnNumberClicked(int)));
    connect(this->sMapperOp, SIGNAL(mapped(QString)), this, SLOT(btnOperatClicked(QString)));

    this->setDesign();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void            MainWindow::setDesign() const
{
    int         currentNb = 9;

    for (int row = 0; row < 3; ++row)
    {
        int     offset = 2;
        for (int col = 0; col < 3; ++col)
        {
            this->ui->gridLayout->addWidget(vecBtn[currentNb - offset], row, col);
            --offset;
        }
        currentNb -= 3;
    }

    this->ui->gridLayout->addWidget(vecBtn[0], 3, 0, 1, 3);
    this->ui->gridLayout->addWidget(vecBtn[13], 0, 3);
    this->ui->gridLayout->addWidget(vecBtn[12], 1, 3);
    this->ui->gridLayout->addWidget(vecBtn[11], 3, 3);
    this->ui->gridLayout->addWidget(vecBtn[10], 2, 3);
}

void            MainWindow::refreshLineResult(const QString str) const
{
    ui->lineEdit->setText(this->ui->lineEdit->text() + str);
}

void            MainWindow::reset()
{
    calcEnd = false;
    currentOperand = 0;
    result = 0;
    opBtnClicked = true;
    ui->lineEdit->clear();
}



/*
 *
 *  SLOTS
 *
 */

void            MainWindow::btnNumberClicked(int nb)
{
    if (this->calcEnd)
        this->reset();
    this->currentOperand = this->currentOperand * 10 + nb;
    this->opBtnClicked = false;
    this->refreshLineResult(QString::number(nb));
}

void            MainWindow::btnOperatClicked(QString op)
{
    if (this->opBtnClicked)
        return ;

    if (op.compare("+") == 0 || op.compare("=") == 0)
    {
        this->result = this->result + this->currentOperand;
        this->currentOperand = 0;
        this->refreshLineResult(" " + op + " ");
        if (op.compare("=") == 0)
        {
            this->refreshLineResult(QString::number(this->result));
            this->calcEnd = true;
        }
    }
    this->opBtnClicked = true;
}
